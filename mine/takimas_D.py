def pi(seqs: list) -> float:
    """nucleotide diversity π"""
    from itertools import combinations
    combs = list(combinations(seqs, r=2))
    n_c = len(combs)
    n_n = len(seqs[0])
    res = 0.0
    for comb in combs:
        pi_ij = 0.0
        for pair in zip(comb[0], comb[1]):
            pi_ij += (pair[0] != pair[1])
        res += (pi_ij/n_n)/n_c
    return res

def theta(seqs: list) -> float:
    import numpy as np
    n_poly_sites = 0
    for n in zip(*seqs):
        n_poly_sites += not len(set(n)) <= 1
    res = n_poly_sites / sum(1./ np.array(range(1,len(seqs[0])-1)) )
    return res


seq1 = "AGGCTGCATC"
seq2 = "AAGCTGCATC"
seq3 = "AGGCTGTACC"
seq4 = "AAGCTGCACC"

x = pi([seq1, seq2, seq3])
# print(x)
y = theta([seq1, seq2, seq3])
print(y)